let registerRouter = require('express').Router();
let registerController = require('../controller/registerController');

registerRouter.post('/register',registerController.registeruser)

module.exports=registerRouter;