var express = require('express')
let bodyParser = require('body-parser');
const mongoose = require("mongoose")
const config = require("./config/config")

const registerRouter=require('./router/registerRouter')
const loginRouter=require('./router/loginRouter')
const logger=require('./config/logger')

const app = express()

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.use(function ( req, res, next) {
    logger.log({
        level: "info",
        url: req.url,
        query : req.query,
        headers: req.headers
    });
     next();
 })


mongoose.connect(`mongodb://${config.db.HOST}/${config.db.NAME}`, { useNewUrlParser: true })
.catch(e => console.log(e))

app.get('/', function (req, res) {
    res.send('Hello World with Express');
})

app.use(function ( req, res, next) {
    logger.log({
        level: "info",
        url: req.url,
        query : req.query,
        headers: req.headers
    });
     next();
 })

app.use(registerRouter);
app.use(loginRouter);



app.use(function (err, req, res, next) {
    logger.log({
        level: "error",
        message: err
    }); 
    if (!err.status) err.status = 500;
       res.status(err.status).json({
        err: err.message
    });
    return next(err);
 });

app.listen(`${config.app.PORT}`);