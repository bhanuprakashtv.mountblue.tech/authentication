const mongoose = require('mongoose');
const joi=require("joi")

const schema = mongoose.Schema;

credentialSchema = new schema({
    name: {
        type: String,
        unique: true,
        required:true
    },
    password: {
        type: String,
        required:true
    }
});

const joiSchema = joi.object().keys({
    username: joi.string().alphanum().required(),
    password: joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
 })
 function validate(data) {
    return joi.validate(data, joiSchema);
 }

credentialModel= mongoose.model("credentials ", credentialSchema)

module.exports={credentialModel,validate}