const bcrypt = require('bcrypt');
const mongoose = require('mongoose');

const {credential} = require('../model/credential');
const config = require('../config/config')
const saltRounds = 10;
const password = config.user.PASSWORD;
mongoose.connect(`mongodb://${config.db.HOST}/${config.db.NAME}`, { useNewUrlParser: true })
    .catch(e => console.log(e))


const createHash = async function () {
    try {
        const hash = await bcrypt.hash(password, saltRounds);
        m = await credential.create({
            name: config.user.USERNAME,
            password: hash,
        })
    }
    catch (err) {
        console.log(err)
    }
}
//createHash();



















// const config=require("dotenv").config();
// const mongoose = require('mongoose');
// const credentialModel=require('../model/credential');


// mongoose.connect(process.env.DB_CONN, { useNewUrlParser: true })
// userObj={
//     name:"bhanu",
//     password:"123456"
// }
// createobj(userObj);

// function createobj(userObj){

//     credentialModel.create({
//         name: userObj.name,
//         password:userObj.password

//     })
// }