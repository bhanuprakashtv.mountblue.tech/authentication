const bcrypt = require('bcrypt');
const { credentialModel, validate } = require('../model/credential');
const config = require('../config/config')
const saltRounds = 10;
token = require('../router/token')
exports.registeruser = async function (req, res, next) {
    try {
        let result = validate(req.body);
        if (result.error)  {
            err ={message :`${result.error}`};
            err.status = 406;
            throw err;
        };

        user = await credentialModel.findOne({ name: req.body.username })

        const hash = await bcrypt.hash(req.body.password, saltRounds);
        user = await credentialModel.create({
            name: req.body.username,
            password: hash,
        })

        tokenid = token(req.body);
        res.header({ xauth: tokenid });
        return res.status(200).send(`user details added`);
    }

    catch (err) {
        next(err)
    }
}