const bcrypt = require('bcrypt');
const {credentialModel,validate} = require('../model/credential');
const config = require('../config/config')
const saltRounds = 10;
token = require('../router/token')
exports.loginuser = async function (req, res, next) {
    try {
        let validationresult = validate(req.body);
        if (validationresult.error)  {
            err ={message :`${validationresult.error}`};
            err.status = 406;
            throw err;
        };
        user = await credentialModel.find({ name: req.body.username })
        //console.log(user);
        if (!user[0]) throw new Error("username not available");
        const result = await bcrypt.compareSync(req.body.password, user[0].password)
        if (!result) throw res.status(401).send(`login failed`);
        tokenid = token(req.body);
        res.header({ xauth: tokenid });
        return res.status(200).send(`login successfull`);
    }
    catch (err) {
        next(err)
    }
}